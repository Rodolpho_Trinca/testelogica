﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste
{
    public class StringInput : IStream
    {
        private char[] input;
        private int index;

        public StringInput(string input)
        {
            this.input = input.ToCharArray();
        }

        public bool Exists(char character)
        {
            var count = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (char.ToLower(input[i]).Equals(char.ToLower(character)))
                {
                    count++;
                    if (count > 1)
                        return true;
                }
            }

            return false;
        }
        public char GetNext() => input[index++];
        public char GetBefore()
        {
            index = index - 2;
            return input[index];
        }
        public bool HasNext() => index < input.Length;
        public bool HasBefore() => index > 1;
    }
}
