﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste
{
    public interface IStream
    {
        char GetNext();
        char GetBefore();
        bool HasNext();
        bool HasBefore();
        bool Exists(char character);
    }
}
