﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste
{
    public class StringStream
    {
        private static char[] vogals = new char[] { 'a', 'e', 'i', 'o', 'u' };
        private IStream input;

        public StringStream(IStream input)
        {
            this.input = input;
        }

        public char FirstChar()
        {
            char vogal = char.MinValue;
            do
            {
                char nextChar;
                do
                {
                    nextChar = input.GetNext();

                    if (IsConsonant(nextChar))
                    {
                        break;
                    }
                    else if (input.HasBefore())
                    {
                        nextChar = input.GetBefore();
                        if (IsConsonant(nextChar))
                        {
                            input.GetNext();
                            break;
                        }
                        input.GetNext();
                        input.GetNext();
                    }
                }
                while (input.HasNext());

                char nextVogal;
                if (input.HasNext())
                    nextVogal = input.GetNext();
                else
                    nextVogal = IsVogal(nextChar) ? nextChar : char.MaxValue;

                if (!IsVogal(nextVogal) || input.Exists(nextVogal))
                {
                    continue;
                }

                vogal = nextVogal;
                break;

            } while (input.HasNext());

            if (!char.IsLetter(vogal))
            {
                throw new Exception("O sistema não encontrou nenhuma vogal");
            }

            return vogal;
        }

        private bool IsVogal(char character)
        {
            for (int i = 0; i < vogals.Length; i++)
            {
                if (char.ToLower(character).Equals(vogals[i]))
                    return true;
            }

            return false;
        }

        private bool IsConsonant(char character)
        {
            return !IsVogal(character);
        }
    }
}
