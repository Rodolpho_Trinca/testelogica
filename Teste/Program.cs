﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste
{
    class Program
    {

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Digite o input:");
                string input = Console.ReadLine();

                //string input = "aAbBABacfe";

                try
                {
                    var stringInput = new StringInput(input);
                    var stringStream = new StringStream(stringInput);
                    var vogal = stringStream.FirstChar();
                    Console.WriteLine($"A primeira vogal encontrada foi: {vogal}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Erro: " + ex.Message);
                }
            }
        }
    }
}
