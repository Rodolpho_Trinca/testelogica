﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Teste;

namespace Teste.Testes
{
    [TestClass]
    public class TestandoALogica
    {
        [TestMethod]
        public void VogalNoFinal()
        {
            var caractere = Testar("aAbBABacfe");
            Assert.AreEqual('e', caractere);
        }

        [TestMethod]
        public void VogalNoInicio()
        {
            var caractere = Testar("Abedeceliopo");
            Assert.AreEqual('i', caractere);
        }

        [TestMethod]
        public void VogalNoMeio()
        {
            var caractere = Testar("laapdoee");
            Assert.AreEqual('o', caractere);
        }

        [TestMethod]
        public void VogalComoSegundoCaractere()
        {
            var caractere = Testar("lapdooee");
            Assert.AreEqual('a', caractere);
        }

        [TestMethod]
        public void NaoTemVogalQueNaoRepita()
        {
            try
            {
                var caractere = Testar("laapdooee");
            }
            catch (Exception ex)
            {
                Assert.AreEqual("O sistema não encontrou nenhuma vogal", ex.Message);
            }
        }

        private char Testar(string input)
        {
            var stringInput = new StringInput(input);
            var stringStream = new StringStream(stringInput);
            return stringStream.FirstChar();
        }
    }
}
